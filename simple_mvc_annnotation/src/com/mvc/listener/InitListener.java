/**
 * @(#)com.mvc.listener.InitListener
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.listener;

import com.mvc.bean.ActionBean;
import com.mvc.config.ActionConfig;
import com.mvc.util.XmlParse;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Map;

/**
 * 监听器
 * @author fusf
 * @version 1.0  2015/6/25 0025
 */
public class InitListener implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        String initparmer=servletContextEvent.getServletContext().getInitParameter("config");
        String real=servletContextEvent.getServletContext().getRealPath("/");

        if(initparmer==null){
            initparmer="/WEB-INF/mvc-config.xml";
        }
       Map<String,ActionBean> map = XmlParse.getMap(real+initparmer);
        servletContextEvent.getServletContext().setAttribute(ActionConfig.ACTION_INFO, map);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
