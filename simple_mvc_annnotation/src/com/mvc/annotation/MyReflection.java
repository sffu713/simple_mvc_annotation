/**
 * @(#)com.mvc.annotation.MyReflection
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.annotation;

import com.mvc.bean.MyClass;
import com.mvc.util.ClassUtil;

import java.lang.reflect.Method;
import java.util.List;

/**
 * 获取注释信息找到转换信息
 * @author fusf
 * @version 1.0  2015/7/22 0022
 */
public class MyReflection {

    /**
     * 根据反射获取对应的类和方法
     * @param actionName
     * @param methodName
     * @return
     * @throws Exception
     */
    public static MyClass getMyClass(String actionName,String methodName) throws Exception {
       MyClass myClass = new MyClass();
        List<Class<?>> classes =  ClassUtil.getClasses("com.mvc.action");
        for(Class cls: classes){
            MyControllerAnnotation myController = (MyControllerAnnotation)cls.getAnnotation(MyControllerAnnotation.class);
            if(actionName.equals(myController.value())) { //类名标识一致
                myClass.setCls(cls);
                Method[] methods = cls.getMethods();
                for(Method m : methods) {
                    if(m.isAnnotationPresent(MyMethodAnnotation.class)){
                        MyMethodAnnotation myMethod = m.getAnnotation(MyMethodAnnotation.class);
                        if(methodName.equals(myMethod.value())) {
                           myClass.setMetod(m);
                            MyReflection.getParamter(m);
                            break;
                        }
                    }
                }
                if(myClass.getMetod()!=null) {//判断跳出循环
                    break;
                }

            }
        }
        return myClass;
    }


    public static void getParamter(Method method) {
        System.out.println(method.getName());
        Class<?>[] clss = method.getParameterTypes();
        for(Class cls:clss) {
        }
    }

}
