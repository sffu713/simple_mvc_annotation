/**
 * @(#)com.mvc.annotation.MyMethod
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 方法的标识
 * @author fusf
 * @version 1.0  2015/7/22 0022
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MyMethodAnnotation {
    String value();
}
