/**
 * @(#)com.mvc.util.XmlParse
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.util;

import com.mvc.bean.ActionBean;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * XML解析
 * @author fusf
 * @version 1.0  2015/6/24 0024
 */
public class XmlParse {


    public static Map<String,ActionBean>  getMap(String path) {
        Map<String,ActionBean> map = new HashMap<String, ActionBean>();
        try {
            Document doc = new SAXReader().read(new File(path));
            Element el = doc.getRootElement();
            Iterator<Element> elist= el.elementIterator();
            while (elist.hasNext()) {
                ActionBean actionBean = new ActionBean();
                Element element = elist.next();
                String name = element.attributeValue("name");
                String classBean = element.attributeValue("class");
                String method = element.attributeValue("method");
                String bean = element.attributeValue("bean");
                actionBean.setName(name);
                actionBean.setClassBean(classBean);
                actionBean.setMethod(method);
                actionBean.setBean(bean);
                map.put(name,actionBean);
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        return map;
    }

}
