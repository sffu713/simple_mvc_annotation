/**
 * @(#)com.mvc.util.FullBean
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.util;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;

/**
 * 对象赋值
 * @author fusf
 * @version 1.0  2015/6/23 0023
 */
public class FullBean {

    public static Object full(String bean,HttpServletRequest request) {
        Object o = null;
        try {
            Class clazz = Class.forName(bean);
            o = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            for(Field f:fields) {
                f.setAccessible(true);
                f.set(o,request.getParameter(f.getName()));
                f.setAccessible(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return o;

    }
}
