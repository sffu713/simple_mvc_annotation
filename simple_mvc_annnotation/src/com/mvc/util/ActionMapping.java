/**
 * @(#)com.mvc.action.ActionMapping
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.util;

import com.mvc.bean.ActionBean;
import com.mvc.config.ActionConfig;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Action转换器
 * @author fusf
 * @version 1.0  2015/6/23 0023
 */
public class ActionMapping {

    /**
     * 获取请求中的Action标识和方法标识，格式user_find.do
     * @param request
     * @return
     */
    public static ActionBean getActionBean(HttpServletRequest request) {
            String path = request.getServletPath();
            String[] paths = path.split("\\.");
            String[] paths2 = paths[0].split("_");
            String action = paths2[0].replace("/","");//action名称
            String method = paths2[1];//method名称

            Map<String, ActionBean> map =  (Map<String,ActionBean>) request.getSession().getServletContext().getAttribute(ActionConfig.ACTION_INFO);
            ActionBean bean = map.get(action);
            if(method!=null&&!"".equals(method)) {
                bean.setMethod(method);//方法不为空
            }
           return bean;
    }

}