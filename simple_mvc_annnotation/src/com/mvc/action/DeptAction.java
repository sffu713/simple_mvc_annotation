/**
 * @(#)com.mvc.action.DeptAction
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.action;

import com.mvc.annotation.MyControllerAnnotation;
import com.mvc.annotation.MyMethodAnnotation;
import com.mvc.bean.Dept;

/**
 * @author fusf
 * @version 1.0  2015/6/23 0023
 */
@MyControllerAnnotation("dept")
public class DeptAction {

    @MyMethodAnnotation("create")
    public void create(Dept dept) {
        System.out.println(dept.getDeptName());
        System.out.println(dept.getId());
    }
}
