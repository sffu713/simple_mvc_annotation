/**
 * @(#)com.mvc.action.UserAction
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.action;

import com.mvc.annotation.MyControllerAnnotation;
import com.mvc.annotation.MyMethodAnnotation;
import com.mvc.bean.User;

/**
 * @author fusf
 * @version 1.0  2015/6/23 0023
 */
@MyControllerAnnotation("user")
public class UserAction {

    @MyMethodAnnotation("find")
    public String find(User user) {
        System.out.println(user.getName());;
        return "WEB-INF/view/error.jsp";
    }
}
