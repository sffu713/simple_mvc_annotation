/**
 * @(#)com.mvc.vo.MessageVO
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.vo;

/**
 * @author fusf
 * @version 1.0  2015/6/23 0023
 */
public class MessageVO {

    private String message;
    private String name;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
