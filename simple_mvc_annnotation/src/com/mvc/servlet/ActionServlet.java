/**
 * @(#)com.mvc.servlet.ActionServlet
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.servlet;

import com.mvc.annotation.MyReflection;
import com.mvc.bean.ActionBean;
import com.mvc.bean.MyClass;
import com.mvc.util.ActionMapping;
import com.mvc.util.FullBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * ActionServlet公共方法
 * @author fusf
 * @version 1.0  2015/6/16 0016
 */
public class ActionServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        ActionBean actionBean = ActionMapping.getActionBean(request);
        String action =  actionBean.getClassBean();
        String method = actionBean.getMethod();
        String bean = actionBean.getBean();
        String name = actionBean.getName();
        Object obean = FullBean.full(bean,request);
        Object resObj = null;
        try {
            MyClass myClass = MyReflection.getMyClass(name, method);
            Object o = myClass.getCls().newInstance();
            Method med = myClass.getMetod();
            resObj = med.invoke(o,obean);


        } catch (Exception e) {
            System.out.println("找不到对应的注解类或者方法");
            e.printStackTrace();
        }
        if(resObj instanceof  String ) {
            RequestDispatcher dis = request.getRequestDispatcher((String) resObj);
            dis.forward(request, response);
        }
    }

}
