/**
 * @(#)com.mvc.config.ActionConfig
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.config;

/**
 * @author fusf
 * @version 1.0  2015/6/23 0023
 */
public class ActionConfig {

    public final static String ACTION_INFO = "action_info";
}
