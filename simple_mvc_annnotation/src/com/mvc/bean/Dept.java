/**
 * @(#)com.mvc.bean.Dept
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.bean;

/**
 * @author fusf
 * @version 1.0  2015/6/23 0023
 */
public class Dept {

    private String id;
    private String deptName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
}
