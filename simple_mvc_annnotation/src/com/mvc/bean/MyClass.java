/**
 * @(#)com.mvc.bean.MyClass
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.bean;

import java.lang.reflect.Method;

/**
 * 获取类的信息
 * @author fusf
 * @version 1.0  2015/7/22 0022
 */
public class MyClass {

    private Class<?> cls;
    private Method metod;


    public Class<?> getCls() {
        return cls;
    }

    public void setCls(Class<?> cls) {
        this.cls = cls;
    }

    public Method getMetod() {
        return metod;
    }

    public void setMetod(Method metod) {
        this.metod = metod;
    }
}
