/**
 * @(#)com.mvc.bean.ActionBean
 *
 * Copyright (c) 2014-2018 厦门民航凯亚有限公司
 *
 * DESC:
 *
 */
package com.mvc.bean;

/**
 * @author fusf
 * @version 1.0  2015/6/25 0025
 */
public class ActionBean {

    private String name;//名字
    private String classBean;//类名，全路径
    private String method;//方法名
    private String bean;//实体类

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassBean() {
        return classBean;
    }

    public void setClassBean(String classBean) {
        this.classBean = classBean;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }
}
